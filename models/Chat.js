const mongoose = require('mongoose'),  
      Schema = mongoose.Schema;

const ChatSchema = new Schema({  
  message: {
    type: String,
  },

  sender: {
    type: String,
    // required: false,
  },
  roomName: {
    type: String,
    // required: false,
  },
  receiver: {
    type: String,
    // required: false,
  },

timestamp: {
  type: Date,
  default: Date.now,
},
}); 

module.exports = mongoose.model('Chat', ChatSchema);  

