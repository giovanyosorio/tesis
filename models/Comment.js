const mongoose = require('mongoose');
const { Schema } = mongoose;
const ObjectId = Schema.ObjectId;
const path = require('path');

const CommentSchema = new Schema({
  medicamento_id: { type: ObjectId },
  email: { type: String },
  name: { type: String },
  comment: { type: String },
  timestamp: { type: Date, default: Date.now },
  user_id: {
    type: ObjectId
  },
  user: {
    type: String,
    required: true,
  },
});

CommentSchema.virtual('image')
  .set(function(image) {
    this._image = image;
  })
  .get(function () {
    return this._image;
  });

module.exports = mongoose.model('Comment', CommentSchema);  