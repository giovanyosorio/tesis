const mongoose = require("mongoose");
const { Schema } = mongoose;

const drogaSchema = new Schema({
  expediente: {
    type: String,
    required: true,
  },
  producto: {
    type: String,
    required: true,
  },
  titular: {
    type: String,
    required: true,
  },
  registrosanitario: {
    type: String,
    required: true,
  },
  fechaexpedicion: {
    type: String,
    required: true,
  },
  fechavencimiento: {
    type: String,
    required: true,
  },
  cantidadcum: {
    type: String,
    required: true,
  },
  unidad: {
    type: String,
    required: true,
  },
  timestamp: {
    type: Date,
    default: Date.now,
  },

  cantidad: { type: String },
  mimetype: { type: String },

  unidadreferencia: { type: String },
  nombrerol: { type: String, default: 0 },

});

module.exports = mongoose.model("Droga", drogaSchema);
