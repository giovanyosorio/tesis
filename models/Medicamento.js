const mongoose = require("mongoose");
const { Schema } = mongoose;

var random = require('mongoose-random');

const medicamentoSchema = new Schema({
  nombre: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  registrosanitario: {
    type: String,
    required: true,
  },
  fechaexpedicion: {
    type: String,
    required: true,
  },
  fechavencimiento: {
    type: String,
    required: true,
  },
  descripcioncomercial: {
    type: String,
    required: true,
  },
  unidadmedida: {
    type: String,
    required: true,
  },
  cantidad: {
    type: String,
    required: true,
  },
  public_id: {
    type: String,
    required: true,
  },
  imageURL: {
    type: String,
    required: true,
  },
  timestamp: {
    type: Date,
    default: Date.now,
  },

  originalname: { type: String },
  mimetype: { type: String },

  path: { type: String },
  views: { type: Number, default: 0 },
  like: { type: Number, default: 0 },
  dislike: { type: Number, default: 0 },
  filename: { type: String, required: true },
  user: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Medicamento", medicamentoSchema);