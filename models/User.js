const mongoose = require('mongoose');
const { Schema } = mongoose;
const bcrypt = require('bcryptjs');


const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    default: true
  },
  date: {
    type: Date,
    default: Date.now
  },
   dia: {
    type: String,
    required: true
},
    mes: {
    type: String,
    required: true
},
  year: {
    type: String,
    required: true
},
resetPasswordToken: {type:String},
resetPasswordExpires: {type:Date},
ResetPassword: {type:Date},
gender: {
  type: String,
  required: true
},sentRequest:[{
  username: {type: String, default: ''}
  }],
  request: [{
    userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    username: {type: String, default: ''}
    }],
  friendsList: [{
    friendId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    friendName: {type: String, default: ''}
    }],
    totalRequest: {type: Number, default:0}
});


UserSchema.methods.encryptPassword= async (password)=>{
   const salt= await bcrypt.genSalt(10);
   const hash= bcrypt.hash(password,salt);
   return hash;
}

UserSchema.methods.matchPassword= async function(password){
return await bcrypt.compare(password,this.password);
};



module.exports = mongoose.model('User', UserSchema);