var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();
var methodOverride = require('method-override');
var session= require('express-session');
const passport = require('passport');
const flash = require('connect-flash');
const uuid = require('uuid/v4');
var cookieSession = require('cookie-session')
var fs = require('fs');
//var colombia = require('./colombia.json');
var url = require('url');
const multer = require('multer');
const exphbs = require('express-handlebars');
const redis = require('redis')
var async = require('async');
var crypto = require('crypto');
//setear nuestra app de express
const ciudadesRouter = require('./routes/ciudades');
let RedisStore = require('connect-redis')(session);
let Chat= require('./models/Chat')
let redisClient = redis.createClient()  

var server = require('http').Server(app);
var io = require('socket.io')(server);
const redisAdapter = require('socket.io-redis');

var indexRouter = require('./routes/index');

require('./database/database')
//require('./sockets')
require('./config/passport');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    name:"sesred",
    secret: 'keyboard cat',
    saveUninitialized: false,
    resave: false,
  })
);
app.use(flash());

//setup event listener





const storage = multer.diskStorage({
  destination: path.join(__dirname, 'public/upload/temp'),
  filename: (req, file, cb, filename) => {
      console.log(file);
      cb(null, uuid() + path.extname(file.originalname));
  }
}) 
app.use(multer({storage}).single('image'));

app.use(logger('dev'));

//midlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'));



app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use("/public", express.static(path.join(__dirname, 'public')));


app.use(function(req, res, next){
  res.io = io;
  res.locals.session = req.session;
  res.locals.user = req.user;

  next();
});


// Global Variables
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});
app.use(cookieParser());
//routes
//app.use('/', indexRouter);
app.use(require('./routes/users'));
app.use(require('./routes/index'));
app.use(require('./routes/perfil'));
app.use(require('./routes/account'));
app.use(require('./routes/profile'));

//app.use('/users', usersRouter);
app.use(require('./routes/medicamentos'));
app.use(require('./routes/chat'));
app.use(require('./routes/autocomplete'));

app.use('/ciudades',ciudadesRouter);

//app.use('/notes',notesRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

io.adapter(redisAdapter({ host: 'localhost', port: 6379 }));


io.on('connection', socket => {


  socket.on('subscribe', function(room) {
    console.log('joining room', room);
    socket.join(room);
  });

  socket.on('send message', async function(data) {
    console.log('sending room post', data.room);
    let newChat= new Chat({message:data.message,roomName:data.room,message:data.message,sender:data.username,receiver:data.receiver});
    io.to(data.room).emit('conversation private post', {
      username:data.username,
      message: data.message,
      receiver:data.receiver

    });
    console.log(data);
    await newChat.save();
    /*socket.broadcast.to(data.room).emit('conversation private post', {
      message: data.message
    });*/
  });
  socket.on('friendRequest', (data)=> {
    console.log('sending room post', data.room);
    io.to(data.room).emit('newFriendRequest', {
      message: data.message,
      username:data.username,
    });
    console.log(data);
  });

  /*
  //socket.join('rinoceronte-gio')
  socket.on('new-user', (room, name) => {
      //socket.join(room)
      socket.to(room).emit('user-connected', name)
  })

  socket.on('chat-message', data => {
      console.log('recibiendo desde chat.js....', data);
  })

  //socket.emit('new-user', 'rinoceronte-gio', 'rinoceronte')

  socket.on('disconnect', () => {
      console.log('usser disconected', socket.id)
  })*/

  /*
  socket.on('send-chat-message', (room, message) => {
      socket.to(room).broadcast.emit('chat-message', { message: message, name: rooms[room].users[socket.id] })
  })
  socket.on('disconnect', () => {
  getUserRooms(socket).forEach(room => {
      socket.to(room).broadcast.emit('user-disconnected', rooms[room].users[socket.id])
      delete rooms[room].users[socket.id]
  })
  })*/
})

//module.exports = app;

module.exports = {app: app, server: server};
