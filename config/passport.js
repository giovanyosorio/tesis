
var express = require('express');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const firebase= require('firebase');
const mongoose = require('mongoose');
const User = require('../models/User'); 
var session= require('express-session');
var app = express();
// var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const  FacebookStrategy  = require('passport-facebook').Strategy;
//  passport.use(new GoogleStrategy({
//   consumerKey: '4595329723-b8n3r08b15rs6ppcqia452l3f99rshbm.apps.googleusercontent.com',
//   consumerSecret: 'Hvh1XtM4vZuM7RqHN3weYq-m',
//   callbackURL: "http://www.midominio.com:3000/auth/google/callback"
// },
//   function(token, tokenSecret, profile, done) {
//       User.findOrCreate({ googleId: profile.id }, function (err, user) {
//         return done(err, user);
//       });
//   }
// ));
passport.use(new FacebookStrategy({
  clientID: '2491100131187858',
  clientSecret: 'ccd488216cc3cf05affe1cedc380f28d',
  callbackURL: '/auth/facebook/callback/',
  profileFields: ['name', 'email', 'link', 'locale', 'timezone', 'gender'],
  passReqToCallback: true
}, (req, accessToken, refreshToken, profile, done) => {
  if (req.user) {
    User.findOne({ facebook: profile.id }, (err, existingUser) => {
      if (err) { return done(err); }
      if (existingUser) {
        console.log('There is already a Facebook account that belongs to you. Sign in with that account or delete it, then link it with your current account.' );
        done(err);
      } else {
        User.findById(req.user.id, (err, user) => {
          if (err) { return done(err); }
          user.facebook = profile.id;
          user.tokens.push({ kind: 'facebook', accessToken });
          user.profile.name = user.profile.name || `${profile.name.givenName} ${profile.name.familyName}`;
          user.profile.gender = user.profile.gender || profile._json.gender;
          user.profile.picture = user.profile.picture || `https://graph.facebook.com/${profile.id}/picture?type=large`;
          user.save((err) => {
            console.log( 'Facebook account has been linked.' );
            done(err, user);
          });
        });
      }
    });
  } else {
    User.findOne({ facebook: profile.id }, (err, existingUser) => {
      if (err) { return done(err); }
      if (existingUser) {
        return done(null, existingUser);
      }
      User.findOne({ email: profile._json.email }, (err, existingEmailUser) => {
        if (err) { return done(err); }
        if (existingEmailUser) {
          console.log('There is already an account using this email address. Sign in to that account and link it with Facebook manually from Account Settings.' );
          done(err);
        } else {
          const user = new User();
          user.email = profile._json.email;
          user.facebook = profile.id;
          user.tokens.push({ kind: 'facebook', accessToken });
          user.profile.name = `${profile.name.givenName} ${profile.name.familyName}`;
          user.profile.gender = profile._json.gender;
          user.profile.picture = `https://graph.facebook.com/${profile.id}/picture?type=large`;
          user.profile.location = (profile._json.location) ? profile._json.location.name : '';
          user.save((err) => {
            done(err, user);
          });
        }
      });
    });
  }
}));
passport.use(new GoogleStrategy({
  clientID: '4595329723-b8n3r08b15rs6ppcqia452l3f99rshbm.apps.googleusercontent.com',
  clientSecret: 'Hvh1XtM4vZuM7RqHN3weYq-m',
  callbackURL: "http://www.midominio.com:3000/auth/google/callback",
  passReqToCallback   : true
},(req, accessToken, refreshToken, params, profile, done) => {
  if (req.user) {
    User.findOne({ google: profile.id }, (err, existingUser) => {
      if (err) { return done(err); }
      if (existingUser && (existingUser.id !== req.user.id)) {
        console.log('There is already a Google account that belongs to you. Sign in with that account or delete it, then link it with your current account.' );
        done(err);
      } else {
        User.findById(req.user.id, (err, user) => {
          if (err) { return done(err); }
          user.google = profile.id;
          user.tokens.push({
            kind: 'google',
            accessToken,
            accessTokenExpires: moment().add(params.expires_in, 'seconds').format(),
            refreshToken,
          });
          user.profile.name = user.profile.name || profile.displayName;
          user.profile.gender = user.profile.gender || profile._json.gender;
          user.profile.picture = user.profile.picture || profile._json.picture;
          user.save((err) => {
            console.log('Google account has been linked.' );
            done(err, user);
          });
        });
      }
    });
  } else {
    User.findOne({ google: profile.id }, (err, existingUser) => {
      if (err) { return done(err); }
      if (existingUser) {
        return done(null, existingUser);
      }
      User.findOne({ email: profile.emails[0].value }, (err, existingEmailUser) => {
        if (err) { return done(err); }
        if (existingEmailUser) {
          console.log('There is already an account using this email address. Sign in to that account and link it with Google manually from Account Settings.' );
          done(err);
        } else {
          const user = new User();
          user.email = profile.emails[0].value;
          user.google = profile.id;
          user.tokens.push({
            kind: 'google',
            accessToken,
            accessTokenExpires: moment().add(params.expires_in, 'seconds').format(),
            refreshToken,
          });
          user.profile.name = profile.displayName;
          user.profile.gender = profile._json.gender;
          user.profile.picture = profile._json.picture;
          user.save((err) => {
            done(err, user);
          });
        }
      });
    });
  }
}
));



passport.use(new LocalStrategy({

  usernameField: 'email'
}, async (email, password,done) => {
  // Match Email's User
  const user = await User.findOne({email: email});
  if (!user) {
    console.log("'Not User found.'");
  } else {
    // Match Password's User
    const match = await user.matchPassword(password);
    if(match) {

     // loginUser(email,password);
      console.log('login user en passport');
      return done(null, user);
    } else {
      console.log("'incorrect password.'");
    }
  }
}));


//autenticarse
function loginUser(email, password) {
  console.log('Loging user ' + email);
  //console.log(session.id);    

	firebase.auth().signInWithEmailAndPassword(email, password)
	.then(function (user) {
    console.log('Credenciales correctas, ¡bienvenido!');
    console.log(email);
	})
	.catch(function (error) {
		console.log(error);
	});
 }

passport.serializeUser((user, done) => {
  done(null, user);
});
//toma un id y genera un usuario
//busca en la base de datos, toma el id y retorna el error
passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    
    done(err, user);  
  });
});

