var express = require("express");
var router = express.Router();
// Models
var User = require("../models/User");
const cloudinary=require('cloudinary')
const path = require("path");
const Medicamento = require("../models/Medicamento");
const methodOverride = require("method-override");
var app = express();
const fs=require('fs-extra')
var bodyParser = require("body-parser");
const { isAuthenticated } = require("../helpers/auth");
const { timeago } = require("../helpers/timeago");
const Comment = require("../models/Comment");
const session = require("express-session");
const Droga = require("../models/Droga");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride("_method"));
app.use(bodyParser.json());
app.use("/public", express.static(path.join(__dirname, "public")));
/* GET home page. */

cloudinary.config({
  cloud_name:'halyfax',
  api_key:'159957662763752',
  api_secret:'_EKI4HAS_aaNjwur--S27c3cFjE'
})

router.get("/medicamentos/add", isAuthenticated, function (req, res, next) {
  res.render("medicamentos/new-medicamento");
});

router.post(
  "/medicamentos/new-medicamento",
  isAuthenticated,
  async (req, res) => {
    const newMedicamento = new Medicamento();
    const result= await cloudinary.v2.uploader.upload(req.file.path)

    const ext = path.extname(req.file.originalname).toLowerCase();
    const targetPath = path.resolve(`public/upload`);
    newMedicamento.public_id=result.public_id;

    newMedicamento.nombre = req.body.nombre;
    newMedicamento.description = req.body.description;
    newMedicamento.registrosanitario = req.body.registrosanitario;
    newMedicamento.fechaexpedicion = req.body.fechaexpedicion;
    // newNote.user=req.user.id;
    newMedicamento.fechavencimiento = req.body.fechavencimiento;
    newMedicamento.descripcioncomercial = req.body.descripcioncomercial;
    newMedicamento.unidadmedida = req.body.unidadmedida;
    newMedicamento.cantidad = req.body.cantidad;
    newMedicamento.filename = req.file.filename;
    newMedicamento.imageURL=result.url;
    //   newMedicamento.path = result.url;
    // newNote.path=' /img/uploads'+req.body;
    newMedicamento.originalname = req.file.originalname;
    newMedicamento.mimetype = req.file.mimetype;
    //newMedicamento.user = req.user.id;
    newMedicamento.user = req.user.username;
    //newMedicamento.user = req.user.username;

    await newMedicamento.save();
    console.log('path')
    console.log(path);
    console.log('result')

    console.log(result);
    console.log('nuevo medicamento')
    console.log(newMedicamento);
    // req.flash('success_msg', 'Note Added Successfully');
    await fs.unlink(req.file.path);
    res.redirect("/medicamentos");
  }
);

router.get("/medicamentos", isAuthenticated, async (req, res) => {
  console.log(req.sessionID);
  console.log("medicamentos");

  const medicamentos = await Medicamento.find({
    user: req.user.username,
  }).sort({ timestamp: -1 });
  res.render("medicamentos/all-medicamentos", { medicamentos });
});

router.get("/medicamentos/edit/:id", isAuthenticated, async (req, res) => {
  const medicamento = await Medicamento.findById(req.params.id);
  //req.flash('success_msg', 'Note Updated Successfully');
  res.render("medicamentos/edit-medicamento", { medicamento });
});

router.post(
  "/medicamentos/edit-medicamento/:id",
  isAuthenticated,
  async (req, res) => {
    const results= await cloudinary.v2.uploader.upload(req.file.path)

    const { nombre, description } = req.body;
   // const { filename } = req.file.filename;
 //  const {filename}=results.original_filename;
    //const { originalname } = req.file.originalname;

    const { path } = req.file.path;
 // const { mimetype } = req.file.mimetype;
//  const { imageURL } = results.url;

    await Medicamento.findByIdAndUpdate(req.params.id, {
      nombre,
      description,
      imageURL:results.url,
      filename:results.original_filename,
     path,
     // originalname,
    });
    console.log('aca debe imprimir')
    console.log(nombre);
    console.log(description);
    console.log(results);
    await fs.unlink(req.file.path);

    
    res.redirect("/medicamentos");
  }
);

router.delete("/medicamentos/delete/:id", isAuthenticated, async (req, res) => {
  await Medicamento.findByIdAndDelete(req.params.id);
  res.redirect("/medicamentos");
});

router.get("/medicamentos/search", isAuthenticated, function (req, res, next) {
  res.render("medicamentos/search-medicamento");
});

router.get("/medicamentos/view/:id", isAuthenticated, async (req, res) => {
  const medicamento = await Medicamento.findById(req.params.id);
  if (medicamento) {
    medicamento.views = medicamento.views + 1;
    await medicamento.save();
    const comment = await Comment.find({ medicamento_id: medicamento._id });
    //req.flash('success_msg', 'Note Updated Successfully');
    res.render("medicamentos/view-medicamento", { medicamento, comment });
  } else {
    res.redirect("/medicamentos");
  }
});

router.get(
  "/medicamentos/find-medicamento",
  isAuthenticated,
  async (req, res) => {
    // const { description } = req.body;
    // const medicamentos= await Medicamento.findOne({description:description});
    //  const medicamentos = await Medicamento.find({user:req.user.id}).sort({ date: "" });

    res.render("medicamentos/find-medicamento");
  }
);
router.get('/finder', async function(req, res, next) {
  // console.log(req.session.id);    
  //res.io.emit("socketToMe", "users");
 
//  const usuarios = await User.find({ }).sort({timestamp: -1});
   res.render("finder");
 
   })
   router.post(
    "/finder/",
    isAuthenticated,
    async (req, res) => {
      const { nombre } = req.body;
      const busqueda = nombre.toLowerCase() || nombre.toUpperCase();
  
      const medicamento = await Medicamento.findOne({ nombre: busqueda });
      if (medicamento) {
        console.log(medicamento);
        console.log(nombre);
        res.render("medicamentos/find-medicamento", { medicamento });
        //  res.redirect("/medicamentos");
        //res.redirect("/medicamentos")
      } else {
        res.send("medicamento no encontrado finderrr");
      }
    }
  );

router.post(
  "/medicamentos/search-medicamento/",
  isAuthenticated,
  async (req, res) => {
    const { description } = req.body;
    const busqueda = description.toLowerCase() || description.toUpperCase();

    const medicamento = await Medicamento.findOne({ description: busqueda });
    if (medicamento) {
      console.log(medicamento);
      console.log(description);
      res.render("medicamentos/find-medicamento", { medicamento });
      //  res.redirect("/medicamentos");
      //res.redirect("/medicamentos")
    } else {
      res.send("medicamento no encontrado");
    }
  }
);
router.post("/medicamentos/comment/:id", isAuthenticated, async (req, res) => {
  const medicamento = await Medicamento.findById(req.params.id);
  if (medicamento) {
    const newComment = new Comment(req.body);
    newComment.medicamento_id = medicamento._id;
    newComment.user = req.user.username;
    await newComment.save();
    console.log(newComment);
    //   console.log(medicamento_id);
    res.redirect("/medicamentos");
  } else {
    res.redirect("/medicamentos");
  }
});
router.post("/medicamentos/likes/:id", isAuthenticated, async (req, res) => {
  //console.log('hola mundo');

  const medicamento = await Medicamento.findById(req.params.id);
  if (medicamento) {
    medicamento.like = medicamento.like + 1;
    await medicamento.save();
    res.redirect("/medicamentos");
  }
});
router.post("/medicamentos/dislike/:id", isAuthenticated, async (req, res) => {
  //console.log('hola mundo');

  const medicamento = await Medicamento.findById(req.params.id);
  if (medicamento) {
    medicamento.like = medicamento.like - 1;
    await medicamento.save();
    res.redirect("/medicamentos");
  }
});


module.exports = router;
