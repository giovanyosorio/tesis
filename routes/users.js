var express = require("express");
var router = express.Router();
var User = require('../models/User');
var passport= require('passport');
var firebase = require('firebase');
var admin = require('firebase-admin');
var flash = require('express-flash');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const bcrypt = require('bcryptjs');

const { isAuthenticated } = require("../helpers/auth");
var session= require('express-session');
const FirebaseAuth = require('firebaseauth');
'use strict'
//const firebase = new FirebaseAuth(process.env.FIREBASE_API_KEY);

const config = {
  apiKey: "AIzaSyBHw-PINAxmRtTXyhmtYtHGWWGkIWJqTzs",
  authDomain: "haly-c1b7d.firebaseapp.com",
  databaseURL: "https://haly-c1b7d.firebaseio.com",
  projectId: "haly-c1b7d",
  storageBucket: "haly-c1b7d.appspot.com",
  messagingSenderId: "914670521249",
  appId: "1:914670521249:web:45c86f75a46a8e9d9819e0"
};
firebase.initializeApp(config);
const auth = firebase.auth();
var provider = new firebase.auth.GoogleAuthProvider();
var app = admin.initializeApp();


router.get('/users/forgot', function(req, res) {
  res.render('users/forgot', {
    user: req.user
  });
});

// User validation failed:
//  resetPasswordToken: Cast to Object failed for value "ccb65bd8f9e47fe968afeb0af6b8520c50d6adbd
//  " at path "resetPasswordToken",
//   resetPasswordExpires: Cast to Object failed for value "1603338507334" at path "resetPasswordExpires"


router.post('/users/forgot', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          console.log('error', 'No account with that email address exists.');
          return res.redirect('/users/forgot');
        }

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var smtpTransport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        auth: {
          user: 'gosoriot3',
          pass: '!fEOCl0iocFd'
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'halyfax@gmail.com',
        subject: 'Node.js Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        console.log('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
        done(err, 'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/users/forgot');
  });
});

router.get('/reset/:token', function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
    }
    res.render('reset', {
      user: req.user
    });
  });
});


router.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {
      User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }
        user.password = bcrypt.hashSync(req.body.password, 10);

        //user.password = req.body.password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          req.logIn(user, function(err) {
            done(err, user);
          });
        });
      });
    },
    function(user, done) {
      var smtpTransport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        auth: {
          user: 'gosoriot3',
          pass: '!fEOCl0iocFd'
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
       console.log('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/users/ingresar');
  });
});




router.get("/users/ingresar", function(req, res, next) {


  if(!req.sessionID){
    console.log(req.sessionID)
    console.log('login by users.js');
   // res.render("/");

  
  }else{
    res.render("users/ingresar");
  
  }

});

router.get('/auth/google', 
passport.authenticate('google',
 { scope: ['profile', 'email', 
 'https://www.googleapis.com/auth/drive', 
 'https://www.googleapis.com/auth/spreadsheets.readonly'],
  accessType: 'offline', prompt: 'consent' }));

router.get('/auth/google/callback', 
passport.authenticate('google',
 { failureRedirect: '/users/ingresar' }), (req, res) => {
  res.redirect(req.session.returnTo || '/medicamentos');
});

router.get('/auth/facebook', 
passport.authenticate('facebook', 
{ scope: ['email', 'public_profile'] }));

router.get('/auth/facebook/callback',
 passport.authenticate('facebook', 
 { failureRedirect: '/login' }),
  (req, res) => {
  res.redirect(req.session.returnTo || '/medicamentos');
});





// router.get('/auth/google',
//   passport.authenticate('google', { scope:
//       [ 'email', 'profile' ] }
// ));

// router.get('/auth/google/callback', 
//   passport.authenticate('google', 
//   { failureRedirect: '/users/ingresar' }),
//   function(req, res) {
//     // Successful authentication, redirect home.
//     res.redirect('/medicamentos');
//   });

router.post('/users/ingresar', passport.authenticate('local', {
  
  successRedirect: '/medicamentos',
  //successRedirect: '/chat',
  failureRedirect: '/users/ingresar',
  failureFlash: true,
  //authWithProvider: auth.signInWithPopup(provider)
 // sessionID: false

}));

router.post('/users/ingresar/google', function(req, res, next){

  var provider = new auth.GoogleAuthProvider();
  provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
  auth().languageCode = 'es';
  auth.signInWithPopup(provider).then((result)=>{
    res.render('/users/google')
  })



});


router.get("/users/registrarse", function(req, res, next) {
  res.render("users/registrarse");
});

router.post("/users/registrarse", async function(req, res, next) {
  const { name, email, password, password2,dia,mes,year,gender,username } = req.body;
  // const name=req.session.name;
  // const email=req.session.email;
  // const password=req.session.password;
  // const password2=req.session.password2;
  // const dia=req.session.dia;
  // const mes=req.session.mes;
  // const year=req.session.year;
  // const gender=req.session.gender;
  // const username=req.session.username;


  if (name.length <= 0) {
    res.send("ingrese el nombre");
  }
  if (password != password2) {
    res.send("las contraseñas no coinciden");
  }
  if (password.length < 4) {
    res.send("contraseña muy corta");
  } else {

    const emailUser= await User.findOne({email:email});
    if(emailUser){
      res.send("email ya existe");
      res.redirect('/users/registrarse');
    }
      const newUser=new User({name,email,password,dia,mes,year,gender,username});
      newUser.password= await newUser.encryptPassword(password);
    await newUser.save();
    createUser(email,password);
    res.redirect('/users/ingresar');

  }

});



router.get('/logout', (req, res, next) => {

  req.logout();
  signoutUser();
  res.redirect('/');  
  
  
 });
/*
router.post('/users/ingresar', passport.authenticate('local', {
  successRedirect: '/chat',
  failureRedirect: '/users/ingresar',
  failureFlash: true
}));

*/

function createUser(email, password) {
	console.log('Creando el usuario con email ' + email);

	firebase.auth().createUserWithEmailAndPassword(email, password)
	.then(function (user) {
    console.log('¡Creamos al usuario!');
    console.log(email);
	})
	.catch(function (error) {
		console.error(error)
	});
}
function signoutUser() {
  firebase.auth().signOut().then(function() {
    console.log('Signed Out');
  }, function(error) {
    console.error('Sign Out Error', error);
  });
};

// Login Process
module.exports = router;
