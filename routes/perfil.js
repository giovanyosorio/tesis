var express = require("express");
var router = express.Router();
const { isAuthenticated } = require("../helpers/auth");
var User = require("../models/User");
var app = express();
const Medicamento = require("../models/Medicamento");

const path = require("path");

app.use("/public", express.static(path.join(__dirname, "public")));



router.get("/perfil/:username",isAuthenticated,async (req, res) => {


    if (!req.session.id) {
      console.log("'Not User found.'");
    }else{

      console.log('aqui debe ser la validacion');
      console.log(`este es tu perfil ${req.session.id}`);

   //   res.render('profile', {user: user});
   res.render('perfil');
    }
});

  router.get('/perfil/usuario/:username', (req, res) => {

console.log(req.params.username)
    User.findOne({username: req.params.username},async (err, user) => {
      if (!user) {
        console.log("'Not User found.'");
      }else{

        console.log('aqui debe ser la validacion');
        const medicamentos = await Medicamento.find({ user: req.params.username }).sort({timestamp: -1});
      console.log(medicamentos)
     res.render('profile',{user,medicamentos});
      }

  });


});

module.exports = router;
