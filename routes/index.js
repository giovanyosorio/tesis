var express = require('express');
var router = express.Router();

var app = express();
const Medicamento = require("../models/Medicamento");
const User = require('../models/User');
const Droga = require("../models/Droga");

var random = require('mongoose-random');


/* GET home page. */
router.get('/', async function(req, res, next) {
 // console.log(req.session.id);    
 //res.io.emit("socketToMe", "users");
 const medicamentos = await Medicamento.find({ }).sort({timestamp:Math.floor(Math.random() * (1 - -1)) -1});


 res.render("index", { medicamentos});

  })


  router.get("/autocompletar/", function (req, res,next) {
    var regex = new RegExp(req.query["term"], "i");
    var query = Medicamento.find({ nombre: regex }, { 'nombre': 1 }).sort({ timestamp: -1 }).limit(20);
    query.exec(function(err,data){
        var result=[];
        if(!err){
          if(data && data.length && data.length>0){
            data.forEach(droga=>{
              let obj={
                id:droga._id,
                label: droga.nombre
              };
              result.push(obj);
             });
           }
        console.log(result)
  res.jsonp(result)
 
         }
           });
          
 });


// router.post(
//   "/medicamentos/search-medicamento/",
//   isAuthenticated,
//   async (req, res) => {
//     const { description } = req.body;
//     const busqueda = description.toLowerCase() || description.toUpperCase();

//     const medicamento = await Medicamento.findOne({ description: busqueda });
//     if (medicamento) {
//       console.log(medicamento);
//       console.log(description);
//       res.render("medicamentos/find-medicamento", { medicamento });
//       //  res.redirect("/medicamentos");
//       //res.redirect("/medicamentos")
//     } else {
//       res.send("medicamento no encontrado");
//     }
//   }
// );
router.get('/about', function(req, res, next) {
  res.io.on('connection',()=>{
    console.log('new connection');
    
  })

  res.render('about', { title: 'about us' });
});

/* GET home page. */

   router.get('/vistas', function(req, res, next) {
  
  
    res.render('vistas', { title: 'about us' });
  });

  //  router.post('/search', async function(req, res, next) {
  //   // console.log(req.session.id);    
  //   //res.io.emit("socketToMe", "users");
   
  //   const usuarios = await User.find({ }).sort({timestamp: -1});
  //    res.render("search",{usuarios} );
   
  //    })
 


module.exports = router;
